﻿namespace CMSTemplates.Api.Tests
{
    using CMSTemplates.Api.controllers;
    using CMSTemplates.Domain;
    using CMSTemplates.Services;

    using Microsoft.AspNetCore.Mvc;
    using Moq;
    using Xunit;

    public class HealthControllerTests
    {
        [Fact]
        public void Index_WhenIsAbleToCheckStatus_ShouldReturnOk()
        {
            // Arrange
            var context = new HealthControllerContext();
            var expectedResult = ApplicationHealthInfo.Hydrate("Test Service", "1.0");

            // Setup
            context.healthServiceMock
                .Setup(service => service.GetApplicationHealthInfo(It.IsAny<string>(), It.IsAny<System.Version>()))
                .Returns(expectedResult);

            // Act
            var result = context.subjectUnderTest.Index();

            // Assert
            Assert.IsType<OkObjectResult>(result);
            Assert.Same(expectedResult, (result as OkObjectResult).Value);
        }

        internal class HealthControllerContext
        {
            public Mock<IHealthService> healthServiceMock;
            public HealthController subjectUnderTest;

            public HealthControllerContext()
            {
                healthServiceMock = new Mock<IHealthService>();
                subjectUnderTest = new HealthController(healthServiceMock.Object);
            }
        }
    }
}
