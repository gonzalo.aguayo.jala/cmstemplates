﻿namespace CMSTemplates.Services
{
    using CMSTemplates.Data;
    using CMSTemplates.Domain;

    using System;

    public class HealthService : IHealthService
    {
        private readonly ICheckSystemDAO checkSystemDAO;

        public HealthService(ICheckSystemDAO checkSystemDAO)
        {
            this.checkSystemDAO = checkSystemDAO;
        }

        public ApplicationHealthInfo GetApplicationHealthInfo(string name, Version version)
        {
            return checkSystemDAO.GetCurrentStatus(name, version.ToString());
        }
    }
}
