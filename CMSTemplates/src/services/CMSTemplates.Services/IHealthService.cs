﻿namespace CMSTemplates.Services
{
    using CMSTemplates.Domain;

    using System;

    public interface IHealthService
    {
        ApplicationHealthInfo GetApplicationHealthInfo(string name, Version version);
    }
}
