﻿namespace CMSTemplates.Data
{
    using CMSTemplates.Domain;

    public class CheckSystemDAO : ICheckSystemDAO
    {
        public ApplicationHealthInfo GetCurrentStatus(string name, string version)
        {
            return ApplicationHealthInfo.Hydrate(name, version);
        }
    }
}
