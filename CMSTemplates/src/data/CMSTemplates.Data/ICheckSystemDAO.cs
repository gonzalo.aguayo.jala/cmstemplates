﻿namespace CMSTemplates.Data
{
    using CMSTemplates.Domain;

    public interface ICheckSystemDAO
    {
        ApplicationHealthInfo GetCurrentStatus(string name, string version);
    }
}
