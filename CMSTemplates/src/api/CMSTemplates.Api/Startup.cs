﻿namespace CMSTemplates.Api
{
    using CMSTemplates.Data;
    using CMSTemplates.Services;

    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    public class Startup
    {
        private readonly IConfiguration configuration;

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddEnvironmentVariables();

            configuration = builder.Build();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(policy =>
            {
                policy.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader();
            });

            app.UseMvc();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddCors()
                .AddMvcCore()
                .AddFormatterMappings()
                .AddJsonFormatters()
                .AddJsonOptions(options =>
                {
                    options
                        .SerializerSettings
                        .Converters
                        .Add(new Newtonsoft.Json.Converters.StringEnumConverter());

                    options
                        .SerializerSettings
                        .NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
                });
        }
    }
}
