﻿namespace CMSTemplates.Api
{
    using System;
    using System.Configuration;
    using System.IO;
    using Microsoft.AspNetCore.Hosting;

    public class Program
    {
        private const string logo = @"
           _____ __  __  _____   _______                   _       _            
          / ____|  \/  |/ ____| |__   __|                 | |     | |           
         | |    | \  / | (___      | | ___ _ __ ___  _ __ | | __ _| |_ ___  ___ 
         | |    | |\/| |\___ \     | |/ _ \ '_ ` _ \| '_ \| |/ _` | __/ _ \/ __|
         | |____| |  | |____) |    | |  __/ | | | | | |_) | | (_| | ||  __/\__ \
          \_____|_|  |_|_____/     |_|\___|_| |_| |_| .__/|_|\__,_|\__\___||___/
                                                    | |                         
                                                    |_|                         
        ";

        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine(logo);

                string url = ConfigurationManager.AppSettings["apiUrl"];

                var host = new WebHostBuilder()
                    .UseKestrel()
                    .UseContentRoot(Directory.GetCurrentDirectory())
                    .UseUrls(urls: url)
                    .UseStartup<Startup>()
                    .Build();

                Console.WriteLine($"Service Running on {url}");
                host.Run();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Unable to start the application {ex}");
                throw;
            }
        }
    }
}
