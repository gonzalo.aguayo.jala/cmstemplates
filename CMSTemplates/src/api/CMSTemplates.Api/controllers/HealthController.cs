﻿namespace CMSTemplates.Api.controllers
{
    using CMSTemplates.Domain;
    using CMSTemplates.Services;

    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using System;
    using System.Configuration;
    using System.Reflection;

    [ApiController]
    public class HealthController : ControllerBase
    {
        private readonly IHealthService healthService;

        public HealthController(IHealthService healthService)
        {
            this.healthService = healthService;
        }


        [HttpGet("[controller]")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult Index()
        {
            try
            {
                var currentApplicationVersion = Assembly.GetExecutingAssembly().GetName().Version;
                var name = ConfigurationManager.AppSettings["name"];

                var applicationHealthInfo = healthService.GetApplicationHealthInfo(name, currentApplicationVersion);
                var response = this.Ok(applicationHealthInfo);

                return response;
            }
            catch (Exception ex)
            {
                Console.WriteLine("There was an error calling to Index", ex);
                throw;
            }
        }
    }
}
